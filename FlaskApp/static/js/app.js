
 $(document).ready(function() {
	
$('#clearAll').click(function(){
   $('input[type="checkbox"]:checked').prop('checked',false);
   $('input[type="radio"]:checked').prop('checked',false);
   document.getElementById('ResultInner').innerHTML = "";
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

$('#ModalReview').on('show.bs.modal', function (event) {
  	var button = $(event.relatedTarget); 
  
 	var cog= [];
	var motor= [];
	var perc= [];
	var vision= [];

  	$.each($("input[name='cognitive']:checked"), function(){            
   	 cog.push($(this).val());
  	});
	$.each($("input[name='motorCheck']:checked"), function(){            
   	 motor.push($(this).val());
  	});
	$.each($("input[name='hearingAbility']:checked"), function(){            
   	 perc.push($(this).val());
  	});
	$.each($("input[name='visionAbility']:checked"), function(){            
   	 vision.push($(this).val());
  	});
  
 	 var cogAbility = $.map(cog, function(value){
  	  return (value);
 	 });
	
	 var motorAbility = $.map(motor, function(value){
  	  return (value);
 	 });
	
	var percAbility = $.map(perc, function(value){
  	  return (value);
 	 });

	var visAbility = $.map(vision, function(value){
  	  return (value);
 	 });

    var modal = $(this)
  	modal.find('.modal-bodyc').html('<strong>The Cognitive Ability selected is: </strong><br>' + cogAbility.join(", "))
	modal.find('.modal-bodym').html('<strong>The Motor Abilities selected are: </strong><br>' + motorAbility.join(", "))
	modal.find('.modal-bodyp').html('<strong>The Perceptual Ability selected is: </strong><br>' + percAbility.join(", "))
	modal.find('.modal-bodyv').html('<strong>The Visual Ability selected is: </strong><br>' + visAbility.join(", "))
	
	

	
	});
	
	function scrollTo(hash) {
		location.hash = "#" + hash;
	}
	
	$('#modalViewMethods').click(function(){
		
		//scrollTo('Result');
		$(window.self).scrollTop($('#Result').offset().top);
	});


 
	$(document).on('click', '.motorSubmit', function() {

		var checks2 = document.getElementsByName("motorCheck");
		var result2 = '';

		
		for(i = 0; i <8; i++) {

			console.log(checks2[i]+"checks");
			if(checks2[i].checked === true) {
				result2 += "Yes";
				//result2 += checks2[i].value;
				console.log(result2+"result");
			}
			else if(checks2[i].checked==false){
				result2 += "No";
				console.log(result2+"result");
			}
		
		}	
		console.log(result2);
        req = $.ajax({
            url : '/start2',
            type : 'POST',
            data: $(result2),
			success : function(data) {
                console.log("ajax success msg(motor) " + result + "--------------");
				console.log(data);
					
            }
			
        }); 

         req.done(function(data) {
            $('result').html(data);
        });
	
		
		
		
		
    });
	$(document).on('click', '.submitButton', function() {
		
		var checks = document.getElementsByName("cognitive");
		var result = '';

		var method = '';
		
		
		for(i = 0; i <3; i++) {
			
			if(checks[i].checked === true) {
				result += checks[i].value + ' ';
				console.log(result);
			}
		
			
		}
		
		var checks2 = document.getElementsByName("motorCheck");
		var result2 = '';

		
		for(i = 0; i <8; i++) {

			console.log(checks2[i]+"checks");
			if(checks2[i].checked === true) {
				result2 += "Yes";
				//result2 += checks2[i].value;
				console.log(result2+"result");
			}
			else if(checks2[i].checked==false){
				result2 += "No";
				console.log(result2+"result");
			}
		
		}
		
		var checks3 = document.getElementsByName("hearingAbility");
		var result3 = '';
		var method = '';
		var difficulty = '';
		for(i = 0; i <3; i++) {

			if(checks3[i].checked === true) {
				result3 += checks3[i].value + ' ';
				console.log(result3);
			}
			

		}

	

        req2 = $.ajax({
            url : '/start2',
            type : 'POST',
            data: $('form').serialize(),
			success : function(data2) {
                console.log("ajax success msg(hearing) " + result3 + "--------------");
				console.log(data2);
					
            }
			
        }); 

         req2.done(function(data) { // To anyone looking at this, I know the code below is very messy and I am going to be cleaning it up and rewriting it really soon. Was just in a rush. -Manuel ☺
				
				//alert(data);
				if (data.includes('Interview (sign language)')){
					method += "<br> <strong>Interview (Sign language):</strong>"
					if (data.includes('Interview (sign language)->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Interview (sign language)->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Interview (sign language)->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Face to Face interviews')){

					method += "<br> <strong>Face to Face Interviews:</strong>"
					if (data.includes('Face to Face interviews->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Face to Face interviews->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Face to Face interviews->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Telephonic interview')){
					method += "<br> <strong>Telephonic Interview:</strong>"
					if (data.includes('Telephonic interview->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Telephonic interview->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Telephonic interview->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Think Aloud protocol (Sign language)')){
					method += "<br> <strong>Think Aloud Protocol (Sign language):</strong>"
					if (data.includes('Think Aloud protocol (Sign language)->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Think Aloud protocol (Sign language)->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Think Aloud protocol (Sign language)->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Think Aloud Protocol')){
					method += "<br> <strong>Think Aloud Protocol:</strong>"
					if (data.includes('Think Aloud Protocol->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Think Aloud Protocol->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Think Aloud Protocol->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Open ended survey (Online survey)')){
					method += "<br> <strong>Open Ended Survey (Online survey):</strong>"
					if (data.includes('Open ended survey (Online survey)->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Open ended survey (Online survey)->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Open ended survey (Online survey)->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
				if (data.includes('Open ended survey (Paper type)')){
					method += "<br> <strong>Open Ended Survey (Paper type):</strong>"
					if (data.includes('Open ended survey (Paper type)->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Open ended survey (Paper type)->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Open ended survey (Paper type)->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Structured questionnaire (Online survey)')){
					method += "<br> <strong>Structured Questionnaire (Online survey):</strong>"
					if (data.includes('Structured questionnaire (Online survey)->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Structured questionnaire (Online survey)->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Structured questionnaire (Online survey)->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Paper prototyping')){
					method += "<br> <strong>Paper Prototyping:</strong>"
					if (data.includes('Paper prototyping->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Paper prototyping->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Paper prototyping->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Focus group')){
					method += "<br> <strong>Focus Group:</strong>"
					if (data.includes('Focus group->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Focus group->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Focus group->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Open Card sorting')){
					method += "<br> <strong>Open Card Sorting:</strong>"
					if (data.includes('Open Card sorting->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Open Card sorting->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Open Card sorting->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Closed Card sorting')){
					method += "<br> <strong>Closed Card Sorting:</strong>"
					if (data.includes('Closed Card sorting->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Closed Card sorting->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Closed Card sorting->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Reverse Card sorting')){
					method += "<br> <strong>Reverse Card Sorting:</strong>"
					if (data.includes('Reverse Card sorting->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Reverse Card sorting->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Reverse Card sorting->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Cultural probe (Camera)')){
					method += "<br> <strong>Cultural Probe (Camera):</strong>"
					if (data.includes('Cultural probe (Camera)->[(u\'Not difficult\',)]')){
						
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Cultural probe (Camera)->[(u\'Moderately difficult\',)]')){
					
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}

					else if (data.includes('Cultural probe (Camera)->[(u\'Difficult\',)]')){
					
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Cultural probe (Diary recording on computer)')){
					method += "<br> <strong>Cultural Probe (Diary recording on computer):</strong>"
					if (data.includes('Cultural probe (Diary recording on computer)->[(u\'Not difficult\',)]')){
						
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"

					}
					else if (data.includes('Cultural probe (Diary recording on computer)->[(u\'Moderately difficult\',)]')){
						
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Cultural probe (Diary recording on computer)->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"

					}
				}
		 		if (data.includes('Cultural probe (Voice recorder)')){
					method += "<br> <strong>Cultural Probe (Voice recorder):</strong>"
					if (data.includes('Cultural probe (Voice recorder)->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"

					}
					else if (data.includes('Cultural probe (Voice recorder)->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"

					}
					else if (data.includes('Cultural probe (Voice recorder)->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Mediated Workshop')){
					method += "<br> <strong>Mediated Workshop:</strong>"
					if (data.includes('Mediated Workshop->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Mediated Workshop->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Mediated Workshop->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
		 		if (data.includes('Retrospective Think Aloud protocol')){
					method += "<br> <strong>Retrospective Think Aloud Protocol:</strong>"
					if (data.includes('Retrospective Think Aloud protocol->[(u\'Not difficult\',)]')){
						method += " Not Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:green\">thumb_up</i>"
					}
					else if (data.includes('Retrospective Think Aloud protocol->[(u\'Moderately difficult\',)]')){
						method += " Moderately Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:yellow\">thumb_up</i>"
					}
					else if (data.includes('Retrospective Think Aloud protocol->[(u\'Difficult\',)]')){
						method += " Difficult "+ "<i class=\"material-icons\" style=\"vertical-align:bottom;font-size:36px;color:red\">thumb_down</i>"
					}
				}
				
		 		if (data.includes("Null!")) {
					method += "<br> The combination you have chosen does not have any recommended methods. Please enter another combination."	
				}
				document.getElementById('ResultInner').innerHTML = "";
				
				document.getElementById('ResultInner').innerHTML += '<div id="idChild"><font size=+0.5>' + method + ' ' + difficulty +'</font></div>';

			
        });
		
		
    });

});







