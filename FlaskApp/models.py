from flask_sqlalchemy import SQLAlchemy
import datetime
db = SQLAlchemy()




class difficulty(db.Model):
    id   = db.Column(db.Integer, primary_key=True, nullable=False)
    methodability = db.Column(db.String(250))
    level  = db.Column(db.String(250))
	
class score(db.Model):
    difficulty   = db.Column(db.String(250), primary_key=True)
    scorenum = db.Column(db.Integer)