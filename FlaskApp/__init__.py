from flask import Flask, render_template, url_for, flash, request, redirect, jsonify, json
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from models import difficulty, score
app = Flask(__name__)


POSTGRES = {
    'user': 'postgres',
    'pw': 'imdcuser',
    'db': 'nickel',
    'host': 'localhost',
    'port': '5432',
}
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES

db = SQLAlchemy(app)

class permutations(db.Model):
    id   = db.Column(db.Integer, primary_key=True, nullable=False)
    method = db.Column(db.String(250))
    cognitivemotor  = db.Column(db.String(250))
	
with app.app_context():
    db.create_all()	

		
@app.route('/')

def homepage():
	
	return render_template("index.html")

@app.route('/about')
def aboutpage():
	title = ""
	paragraph = [" Welcome to the NICKEL Web app"]
	
	pageType = 'about'
	
	return render_template("index.html", title=title, paragraph=paragraph, pageType=pageType)
	

@app.route('/start/', methods=['GET', 'POST'])
def start():
	
	title = ""
	paragraph = ["Cognitive Motor"]
	
	pageType = 'start'
	userInput = request.args.get('data')
	jsdata = None
	if userInput:
		jsdata = "ajax get successful"
		return jsonify(jsdata)
	
	
	methodsQuery = db.engine.execute("select method from permutations")
	motorQuery = db.engine.execute("select cognitivemotor from permutations")
	motorList = motorQuery.fetchall()
	data = request.stream.read()
	methodList = methodsQuery.fetchall()
	data2 = request.stream.read()
	fullQuery = db.engine.execute("select * from permutations")
	selectAll = fullQuery.fetchall()
	selectAllAsString = request.stream.read()
	return render_template("start.html", title=title, paragraph=paragraph, pageType=pageType,data=data, methodList=json.dumps(str(methodList)),motorList=json.dumps(str(motorList)),selectAll=selectAll,selectAllAsString=selectAllAsString, jsdata = jsdata)

@app.route('/result/')
def resultPage():
	title = ""
	paragraph = ["Results"]
	pageType = 'result'

	return render_template("result.html", title=title, paragraph=paragraph, pagetType=pageType)

	
@app.route('/tutorial/', methods=['GET', 'POST'])
def tutorial():
	testQuery = None 
	title = ""
	paragraph = ["Tutorial"]
	
	pageType = 'tutorial'
	
	
	return render_template("tutorial.html", title=title, paragraph=paragraph, pageType=pageType)

	
@app.route('/login/', methods=['GET', 'POST'])
def login(): 
	title = ""
	paragraph = ["Login"]
	
	pageType = 'login'
	
	
	return render_template("login.html", title=title, paragraph=paragraph)
	
@app.route('/instruction/', methods=['GET', 'POST'])
def instruction(): 
	title = ""
	paragraph = ["Instructions"]
	pageType = 'instruction'
	return render_template("instruction.html", title=title, paragraph=paragraph)

	
	
@app.route('/usermanual/', methods=['GET', 'POST'])
def usermanual(): 
	title = ""
	paragraph = ["user manual"]
	
	pageType = 'user manual'
	
	
	return render_template("user_manual.html", title=title, paragraph=paragraph)

@app.route('/user/', methods=['GET', 'POST'])
def user():
	testQuery = None 
	title = ""
	paragraph = ["Manual"]
	pageType = 'Manual'
	
	
	return render_template("user.html", title=title, paragraph=paragraph, pageType=pageType)



@app.route('/start2', methods=['GET', 'POST'])	
def start2():
	cogResult = request.form['cognitive']
	
	
	motorResult = request.form.getlist('motorCheck')
	motorResultStr = str(motorResult)
	
	hearResult = request.form['hearingAbility']
	userSelects = cogResult  # gonna use this as a string of all the selections made that will get sent to the db for the query
	
	visualResult = request.form['visionAbility']
	
	
	#Since the database holds the values related to motor as strings like 'YesNoNoYes..' attached to what cognitive is, im going to do a check
	#seperately for each motor value on motor result to build a string of Yes/No values.
	# there definitely is a smarter way to do this btw --manuel
	if motorResultStr.find("Mouse") == -1:
		userSelects += "No"
	else:
		userSelects += "Yes"
	if motorResultStr.find("Touchscreen") == -1:
		userSelects += "No"
	else:
		userSelects += "Yes"
	if motorResultStr.find("Sign") == -1:
		userSelects += "No"
	else:
		userSelects += "Yes"
	
	if motorResultStr.find("Keyboard") == -1:
		userSelects += "No"
	else:
		userSelects += "Yes"
	if motorResultStr.find("Alternative") == -1:
		userSelects += "No"
	else:
		userSelects += "Yes"
	if motorResultStr.find("Verbal") == -1:
		userSelects += "No"
	else:
		userSelects += "Yes"
	
	if motorResultStr.find("Eye-movement") == -1:
		userSelects += "No"
	else:
		userSelects += "Yes"
	if motorResultStr.find("Writing") == -1:
		userSelects += "No"
	else:
		userSelects += "Yes"
			
	
	query = "select method from permutations where cognitivemotor = '" + userSelects +"';"
	queryFetch = db.engine.execute(query)
	queryPrint = queryFetch.fetchall()
	queryStr = str(queryPrint)
	finalResult = [] #used to query the database with method+hearing, needs all user selections. ex. "Face to Face InterviewsHearing"
	completeList = [] # sending this out
	finalResultVisual = []
		
	if queryStr.find("Face to Face interviews") != -1:
		finalResult.append("Face to Face interviews" )
		completeList.append("Face to Face interviews" )
	if queryStr.find("Interview (Sign language)") != -1:
		finalResult.append("Interview (Sign language)")
		completeList.append("Interview (Sign language)")
	if queryStr.find("Telephonic interview") != -1:
		finalResult.append("Telephonic interview")
		completeList.append("Telephonic interview")
	if queryStr.find("Think Aloud protocol (Sign language)") != -1:
		finalResult.append("Think Aloud protocol (Sign language)")
		completeList.append("Think Aloud protocol (Sign language)")
	if queryStr.find("Think Aloud Protocol") != -1:
		finalResult.append("Think Aloud Protocol")
		completeList.append("Think Aloud Protocol")
	if queryStr.find("Open ended survey (Online survey)") != -1:
		finalResult.append("Open ended survey (Online survey)")
		completeList.append("Open ended survey (Online survey)")	
	if queryStr.find("Open ended survey (Paper type)") != -1:
		finalResult.append("Open ended survey (Paper type)")
		completeList.append("Open ended survey (Paper type)")
	if queryStr.find("Structured questionnaire (Online survey)") != -1:
		finalResult.append("Structured questionnaire (Online survey)")
		completeList.append("Structured questionnaire (Online survey)")
	if queryStr.find("Paper prototyping") != -1:
		finalResult.append("Paper prototyping")
		completeList.append("Paper prototyping")
	if queryStr.find("Focus group") != -1:
		finalResult.append("Focus group")
		completeList.append("Focus group")
	if queryStr.find("Open Card sorting") != -1:
		finalResult.append("Open Card sorting")
		completeList.append("Open Card sorting")
	if queryStr.find("Closed Card sorting") != -1:
		finalResult.append("Closed Card sorting")
		completeList.append("Closed Card sorting")
	if queryStr.find("Reverse Card sorting") != -1:
		finalResult.append("Reverse Card sorting")
		completeList.append("Reverse Card sorting")
	if queryStr.find("Cultural probe (Camera)") != -1:
		finalResult.append("Cultural probe (Camera)")
		completeList.append("Cultural probe (Camera)")
	if queryStr.find("Cultural probe (Diary recording on computer)") != -1:
		finalResult.append("Cultural probe (Diary recording on computer)")
		completeList.append("Cultural probe (Diary recording on computer)")
	if queryStr.find("Cultural probe (Voice recorder)") != -1:
		finalResult.append("Cultural probe (Voice recorder)")
		completeList.append("Cultural probe (Voice recorder)")
	if queryStr.find("Mediated Workshop") != -1:
		finalResult.append("Mediated Workshop")
		completeList.append("Mediated Workshop")
	if queryStr.find("Retrospective Think Aloud protocol") != -1:
		finalResult.append("Retrospective Think Aloud protocol")
		completeList.append("Retrospective Think Aloud protocol")
		
	finalResultVisual = list(finalResult)
	queryStr2 = queryStr
	
	difficultyPriority = [ "[(u'Difficult',)]", "[(u'Moderately difficult',)]", "[(u'Not difficult',)]" ]
	
	for x in range(len(finalResult)):
		finalResult[x] += str(hearResult)
		queryDifficultyH = "select level from difficulty where methodability = '" + finalResult[x] +"';"
		queryExecute = db.engine.execute(queryDifficultyH).fetchall()
		difficultyHearing = str(queryExecute)
		queryStr += " " + difficultyHearing
		#completeList[x] += "->" + str(queryDiffStr)

		finalResultVisual[x] += str(visualResult)
		queryDifficultyV = "select level from difficulty where methodability = '" + finalResultVisual[x] +"';"
		queryExecute2 = db.engine.execute(queryDifficultyV).fetchall()
		difficultyVisual = str(queryExecute2)
		queryStr2 += " " + difficultyVisual
		
		if difficultyPriority.index(str(difficultyHearing)) <= difficultyPriority.index(str(difficultyVisual)):
			completeList[x] += "->" + str(difficultyHearing)
		elif difficultyPriority.index(str(difficultyHearing)) > difficultyPriority.index(str(difficultyVisual)):
			completeList[x] += "->" + str(difficultyVisual)
		
	if completeList == [] :
		completeList.append("Null!")
	return json.dumps({'databaseOutput':str(completeList)}); #used to be -> 'databaseOutput':queryStr , change back if messes up
        
      #  return json.dumps({'databaseOutput': 'Combination not found, please try another combination or add a new method.'});
       
if __name__=="__main__":
	app.debug = True
	app.run()
